# frozen_string_literal: true

require 'gitlab'
require 'date'
require_relative '../api_retry'

module ReleasePosts
  class MergeRequest
    include ApiRetry
    include Helpers

    attr_reader :iid, :project_id, :labels

    def initialize(iid, project_id, labels)
      @iid = iid
      @project_id = project_id
      @labels = labels || []
      get_changes
      get_file_contents(deprecation_change_path)
    end

    def title
      @contents_to_yaml["title"]
    end

    def description
      @contents_to_yaml["body"]
    end

    def date
      DateTime.parse(merged_at).rfc822
    end

    def merged_at
      gitlab_merge_request.merged_at
    end

    def to_s
      "- [!#{iid}](#{link}) #{title}"
    end

    def link
      "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/#{iid}"
    end

    def author
      gitlab_merge_request.author.name
    end

    private

    def gitlab_merge_request
      api_retry do
        @gitlab_merge_request ||= Gitlab.merge_request(project_id, iid)
      end
      @gitlab_merge_request
    end

    def deprecation_change_path
      get_changes.detect { |change| change.new_path.include?("data/deprecations/") }.new_path
    end

    def get_file_contents(path)
      log_info("Getting file contents for #{path}...")
      return @contents_to_yaml unless @contents_to_yaml.nil?

      begin
        api_retry do
          @file_contents = Gitlab.file_contents(project_id, path, "master")
        end

        contents_to_yaml(@file_contents)
      rescue Gitlab::Error::Error => e
        raise e unless e.response_status == 404

        destroy
      end
    end

    def contents_to_yaml(contents)
      @contents_to_yaml ||= YAML.safe_load(contents).first
    end

    def get_changes
      return @changes if defined?(@changes)

      log_info("Getting changes for MR IID #{iid}...")
      api_retry do
        @changes = Gitlab.merge_request_changes(project_id, iid).changes
      end

      @changes
    end
  end
end
